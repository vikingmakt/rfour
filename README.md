# Rfour

Used by the client to request a service

## Methods

### request

Create a promise and send the message to the message.

+ request
    + future -> **future that will receive the response**
    + body -> **payload to be sent**
    + exchange -> **exchange to send the message**
    + rk -> **routing key for the message**
    + headers -> **headers to be send with the message** (optional)

```python
from rask.rmq import RMQ
from rfour import RFour

...

def start(self):
    rmq = RMQ('amqp://localhost')
    rfour = Rfour(rmq)
    rfour.request(
        future=self.ioengine.future(on_respond),
        body='{"name": "Felipe"}',
        exchange='wrio',
        rk='name.save',
        headers={
            'firstname':True
        }
    )
```

---

# Response

Used by the service to respond a request, can be used as a dedicated object or inherit the class

## Attributes

+ datetime -> **Returns a timestamp**

+ utcode -> **UTCode instance, can be redefined**

## Methods

### push

Parse and send the response of the service

+ push
    + body -> **payload of the response**
    + headers -> **headers of the response**
    + channel -> **channel that will be used to push**

```python
from rask.rmq import ack
from rfour import Response

class GenericService(Response):
    def __init__(self,channel):
        self.channel = channel.result().channel
        self.channel.basic_consumer(
            consumer_callback=self.on_msg,
            queue='name_save'
        )

    def do_something(self,msg,headers,ack):
        ...
        headers['status-code'] = 'ok'
        self.ioengine.loop.add_callback(
            self.push,
            body='saved',
            headers=headers,
            channel=self.channel
        )
        ack.set_result(True)
        return True

    def on_msg(self,channel,method,properties,body):
        def on(_):
            self.ioengine.loop.add_callback(
                self.do_something,
                msg=_.result(),
                headers=properties.headers,
                ack=self.ioengine.future(ack(channel,method))
            )
            return True

        self.utcode.decode(body,self.ioengine.future(on))
        return True
```

### response

Only parse the data to the default format of rfour

+ response
    + body -> **payload of the response**
    + headers -> **headers of the response**
    + future -> **future to receive the data**

```python
from rfour import Response

class GenericService(Response):
    def __init__(self,channel):
        self.channel = channel.result().channel
        self.channel.basic_consumer(
            consumer_callback=self.on_msg,
            queue='name_save'
        )

    def prepare_data(self,msg,headers,ack):
        ...
        headers['status-code'] = 'pending'
        def on_data(result):
            print result.result()
            # {
            #     'body':'ut:s3:nok',
            #     'headers':{
            #         'status-code':'pending',
            #         'rfour':True,
            #         'response':True,
            #         'response-datetime':'1479854307.4',
            #         'response-etag':'etag-code'
            #     }
            # }

        self.ioengine.loop.add_callback(
            self.response,
            body='nok',
            headers=headers,
            future=self.ioengine.future(on_data)
        )
        return True

    def on_msg(self,channel,method,properties,body):
        def on(_):
            self.ioengine.loop.add_callback(
                self.prepare_data,
                msg=_.result(),
                headers=properties.headers,
                ack=self.ioengine.future(ack(channel,method))
            )
            return True

        self.utcode.decode(body,self.ioengine.future(on))
        return True
```

### validate_headers

Validate if the headers has the keys **rfour**, **request**, **request-etag**, **request-reply-exchange** and **request-reply-rk**, raising an exception in case of something missing

+ validate_headers
    + headers -> **headers to be validated**

```python
from rfour import Response

class GenericService(Response):
    def valid_message(self):
        headers = {
            'rfour':True,
            'request':True,
            'request-etag':'etag-1234567',
            'request-reply-exchange':'test',
            'request-reply-rk':'name.save.response'
        }
        self.validate_headers(headers)
        # valid message
        return True

    def invalid_message(self):
        headers = {
            'rfour':True,
            'request':True,
            'request-reply-exchange':'test',
            'request-reply-rk':'name.save.response'
        }
        self.validate_headers(headers)
        # raises KeyError
        return True
```

## Changelog

|Version|Description|
|----|----|
|0.0.5|Returning 404 if message not routed|
|0.0.4|Headers and body splitted and others small things|
|0.0.3|Response.push bad argument fixed|
|0.0.2|Accepting request and sending response|
|0.0.1|Start of something awesome|
